#!/usr/bin/env libtbx.python
import os
import sys
import pandas
import ast

data_dir = sys.argv[1]
assert data_dir.endswith("data_222") or data_dir.endswith("data_222/")
if not os.path.isdir(data_dir):
    raise OSError("Directory doenst exists, please input path to data_222")

full_dir = os.path.abspath(data_dir)

df_name = os.path.join(full_dir, "8_stage2_7_gathered_trimmed.tsv")
df = pandas.read_csv(df_name, sep="\t")

df.exp_name = [os.path.join(full_dir, f) for f in df.exp_name]
df.spectrum_filename = [os.path.join(full_dir, f) for f in df.spectrum_filename]
df.predictions = [os.path.join(full_dir, f) for f in df.predictions]

outname = df_name.replace(".tsv", ".pkl")
df.Amats = [ast.literal_eval(v) for v in df.Amats]
df.ncells = [ast.literal_eval(v) for v in df.ncells]
df.ncells_def = [ast.literal_eval(v) for v in df.ncells_def]
df.eta_abc = [ast.literal_eval(v) for v in df.eta_abc]

df.lam0 = 0
df.lam1 = 1

df.to_pickle(outname)
print("Wrote stage2 input pickle: %s" % outname)
