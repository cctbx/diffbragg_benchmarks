
import glob
import sys
import os
from pylab import *
from dials.array_family import flex
from joblib import Parallel, delayed

#ggplot()
#xkcd()

NJ=1


def xy_to_polar(refl,DET, dials=False):
    x, y, _ = refl["xyzobs.px.value"]
    if dials:
        xcal, ycal, _ = refl["dials.xyzcal.px"]
    else:
        xcal, ycal, _ = refl["xyzcal.px"]

    pid = refl['panel']
    panel = DET[pid]
    x,y = panel.pixel_to_millimeter((x,y))
    xcal,ycal = panel.pixel_to_millimeter((xcal,ycal))

    xyz_lab = panel.get_lab_coord((x,y))
    xyz_cal_lab = panel.get_lab_coord((xcal, ycal))

    diff = np.array(xyz_lab) - np.array(xyz_cal_lab)

    xy_lab = np.array((xyz_lab[0], xyz_lab[1]))
    rad = xy_lab / np.linalg.norm(xy_lab)
    tang = np.array([-rad[1], rad[0]])

    rad_component = abs(np.dot(diff[:2], rad))
    tang_component = abs(np.dot(diff[:2], tang))
    pxsize = panel.get_pixel_size()[0]
    return rad_component/pxsize, tang_component/pxsize


def main(jid, njobs):

    from dxtbx.model import ExperimentList
    if len(sys.argv)==2:
        curr_path = os.path.dirname(__file__)
        detpath = os.path.join(curr_path, "../AD_SE_13_222/data_222/Jungfrau_model.json")
        DET = ExperimentList.from_file(detpath, False)[0].detector
    elif len(sys.argv)==3:
        expt_name = sys.argv[2]
        DET = ExperimentList.from_file(expt_name, False)[0].detector

    fnames = glob.glob("%s/refls/rank*/*.refl" % sys.argv[1])
    print("%d fnames" % len(fnames)) 
    assert fnames
    all_d,all_d2 = [],[]
    all_r,all_r2 = [],[]
    all_t,all_t2 = [],[]
    reso = []
    for i_f, f in enumerate(fnames):
        if i_f % njobs != jid:
            continue
        R = flex.reflection_table.from_file(f)
        if len(R)==0:
            continue
        xyobs = R['xyzobs.px.value'].as_numpy_array()[:,:2]
        xycal = R['xyzcal.px'].as_numpy_array()[:,:2]
        reso += list( 1./np.linalg.norm(R['rlp'], axis=1))
        xycal2 = R['dials.xyzcal.px'].as_numpy_array()[:,:2]
        d = np.sqrt(np.sum( (xyobs -xycal)**2, 1))
        d2 = np.sqrt(np.sum( (xyobs -xycal2)**2, 1))
        all_d += list(d)
        all_d2 += list(d2)
        rad,theta = zip(*[xy_to_polar(R[i_r],DET,dials=False) for i_r in range(len(R))])
        rad2,theta2 = zip(*[xy_to_polar(R[i_r],DET,dials=True) for i_r in range(len(R))])
        all_r += list(rad)
        all_r2 += list(rad2)
        all_t += list(theta)
        all_t2 += list(theta2)

        print(i_f)
    return all_d, all_d2, all_r, all_r2, all_t, all_t2, reso

results = Parallel(n_jobs=NJ)(delayed(main)(j,NJ) for j in range(NJ))

all_d, all_d2, all_r, all_r2, all_t, all_t2, reso = [],[],[],[],[],[],[] #zip(*results)
for d,d2,r,r2,t,t2, dspacing in results:
    all_d += d
    all_d2 += d2
    all_r += r
    all_r2 += r2
    all_t += t
    all_t2 += t2
    reso += dspacing

nbin = 10
bins = [ b[0]-1e-6 for b in np.array_split(np.sort(reso), nbin)] + [max(reso)+1e-6]
digs = np.digitize(reso, bins)

all_d = np.array(all_d)
all_d2 = np.array(all_d2)
all_r = np.array(all_r)
all_r2 = np.array(all_r2)
all_t = np.array(all_t)
all_t2 = np.array(all_t2)
reso = np.array(reso)
ave_d, ave_d2, ave_r, ave_r2, ave_t, ave_t2, ave_res =[],[],[],[],[],[],[]
for i_bin in range(1, nbin+1):
    sel = digs==i_bin
    ave_d.append( np.median(all_d[sel]))
    ave_d2.append( np.median(all_d2[sel]))
    ave_r.append( np.median(all_r[sel]))
    ave_r2.append( np.median(all_r2[sel]))
    ave_t.append( np.median(all_t[sel]))
    ave_t2.append( np.median(all_t2[sel]))
    
    ave_res.append( np.median(reso[sel]))

print("overall diffBragg pred offset: %.5f pixels" %np.median(all_d))
print("overall DIALS pred offset: %.5f pixels" %np.median(all_d2))

from tabulate import tabulate
print(tabulate(
    zip(*[ave_res,ave_d, ave_d2, ave_r, ave_r2, ave_t, ave_t2]),
    floatfmt=".3f",
    headers=("res (Ang)", "db (abs)" , "DIALS (abs)", 'db (rad)', "DIALS (rad)", "db (tang)", "DIALS (tang)")))

for vals, vals2, title in [(ave_d, ave_d2, "overall"), (ave_r, ave_r2, "radial component"), (ave_t, ave_t2, "tangential component")]:

    figure()
    gca().set_title(title)
    plot(vals[::-1], color='chartreuse', marker='s', mec='k')
    plot(vals2[::-1], color="tomato", marker='o', mec='k')
    xticks = range(nbin)
    xlabels = ["%.2f" % r for r in ave_res]
    gca().set_xticks(xticks)
    gca().set_xticklabels(xlabels[::-1], rotation=90)
    gcf().set_size_inches((5,4))
    subplots_adjust(bottom=0.2, left=0.15, right=0.98, top=0.9)
    gca().tick_params(labelsize=10, length=0) # direction='in')
    grid(1, color="#777777", ls="--", lw=0.5)
    xlabel("resolution ($\AA$)", fontsize=11, labelpad=5)
    ylabel("prediction offset (pixels)", fontsize=11)
    leg = legend(("diffBragg","DIALS"), prop={"size":10})
    fr = leg.get_frame()
    fr.set_facecolor("bisque")
    fr.set_alpha(1)
    gca().set_facecolor("gainsboro")

show()

