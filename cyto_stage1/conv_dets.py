
from dxtbx.model import Detector, Panel, ExperimentList, Experiment
import dxtbx

loader = dxtbx.load("/global/cfs/cdirs/m3562/der/master_files/run_000795.JF07T32V01_master.h5")
D = loader.get_detector(0)

i_to_pid = {}
for i, (a,b) in enumerate(loader._raw_data._datalists[0]._all_slices):
    if i % 8==0:
        pid = int(a.start/512)
        print(a.start, "pid=%d" %pid)
        i_to_pid[i] = pid

import numpy as np
panels = [None]*32
for pid in range(len(D)):
    orig = np.array(D[pid].get_origin())
    fast = np.array(D[pid].get_fast_axis())
    slow = np.array(D[pid].get_slow_axis())
    px = D[pid].get_pixel_size()[0]
    # need to move out 1 pixel in either direction to get true origin
    new_orig =orig - fast*px - slow*px
    print(orig, new_orig)
    if pid % 8==0:
        pdict = D[pid].to_dict()
        pdict["origin"] = tuple(new_orig)
        pdict["fast_axis"] = tuple(fast)
        pdict["slow_axis"] = tuple(slow)
        pdict["image_size"] = 1030,514
        pid_new = i_to_pid[pid]
        pdict["name"] = "M%d" % pid_new
        PP = Panel.from_dict(pdict)
        panels[pid_new] = PP

newD = Detector()
for PP in panels:
    print(PP.get_name())
    newD.add_panel(PP)

El2 = ExperimentList()
E = Experiment()
E.detector = newD
El2.append(E)
El2.as_file("dxtbx_geom2.expt")
