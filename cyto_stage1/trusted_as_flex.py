import h5py
from simtbx.diffBragg import utils

h = h5py.File('trusted_Py3.hdf5', 'r')

mask_names = {"mask":"trusted_Py3.pkl",
    "newmask": "newmask_withbad.pkl",
    "bigPix2": "bigPix_forDials.pkl",
    "bigPix3": "bigPix_forDiffB.pkl"
}

for dsetname, filename in mask_names.items():
    mask = h[dsetname][()]
    utils.save_numpy_mask_as_flex(mask, filename)
    print("Wrote %s" % filename)


