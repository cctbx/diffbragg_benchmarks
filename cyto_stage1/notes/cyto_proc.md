# One more round of filtering (again)

proc:

```
DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 simtbx.diffBragg.hopper  hopper8_watchmans_ease.phil  outdir=watchmans_ease/hopper8/
```

filter:

````
srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 libtbx.python filt_refls.MPI.py  "watchmans_ease/hopper8/pandas/*/*"  watchmans_ease/hopper8/filt.pl --thresh 7 --ers watchmans_ease/hopper8/filt.txt --minRef 10
````

reproc (took 1 hour on 4 perlmutter nodes):

```
DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 simtbx.diffBragg.hopper  hopper8_watchmans_ease.phil  outdir=watchmans_ease/hopper8/reproc  exp_ref_spec_file= watchmans_ease/hopper8/filt.txt  best_pickle=watchmans_ease/hopper8/filt.pkl
```

![image-20220425040731991](/home/teru/.var/app/io.typora.Typora/config/Typora/typora-user-images/image-20220425040731991.png)

filter2:

```
srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 libtbx.python filt_refls.MPI.py  "watchmans_ease/hopper8/reproc/pandas/*/*"  watchmans_ease/hopper8/reproc/filt.pkl --thresh 5 --ers watchmans_ease/hopper8/reproc/filt.txt --minRef 10
```

reproc2

```
time DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 simtbx.diffBragg.hopper  hopper8_watchmans_ease.phil  outdir=watchmans_ease/hopper8/reproc2  exp_ref_spec_file=watchmans_ease/hopper8/reproc/filt.txt  best_pickle=watchmans_ease/hopper8/reproc/filt.pkl
```

![image-20220425040646327](/home/teru/.var/app/io.typora.Typora/config/Typora/typora-user-images/image-20220425040646327.png)

reproc3

```
time DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 simtbx.diffBragg.hopper  hopper8_watchmans_ease.phil  outdir=watchmans_ease/hopper8/reproc3  exp_ref_spec_file=watchmans_ease/hopper8/reproc2/filt.txt  best_pickle=watchmans_ease/hopper8/reproc2/filt.pkl  ignore_existing=True
```



ensemble:

```
srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 libtbx.python filt_refls.MPI.py  "watchmans_ease/hopper8/reproc2/pandas/*/*"  watchmans_ease/hopper8/reproc2/filt_4ens.pkl --thresh 5  --minRef 10 --mind 3

sbatch  watchmans_ease_ensemble.PM.sh
```

improves geometry a bit, also this was not converged 

![image-20220426004920929](/home/teru/.var/app/io.typora.Typora/config/Typora/typora-user-images/image-20220426004920929.png)





predictions (26 min on 4 PM nodes): 

```
DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=16 -c2 hopper hopper8_perRoi_watchmanes_ease.phil exp_ref_spec_file=watchmans_ease/hopper8/reproc3/sase_pred/pred.txt  outdir=watchmans_ease/hopper8/reproc3/perRoi-a first_n=3800 ignore_existing=True use_restraints=False num_devices=4 best_pickle=watchmans_ease/hopper8/reproc3/sase_pred/pred.pkl

```



perRoi:

```
DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=16 -c2 hopper hopper8_perRoi_watchmanes_ease.phil  outdir=watchmans_ease/hopper8/reproc3/perRoi-a first_n=3800 ignore_existing=True use_restraints=False num_devices=4
```



Errors:

```
DIFFBRAGG_USE_CUDA=1 srun --cpus-per-gpu=2 --gpus-per-node=4 -N4 --tasks-per-node=8 -c2 libtbx.python errors.py  perRoi-b-cont.phil  watchmans_ease/hopper8/reproc3/perRoi-b/ watchmans_ease/hopper8/reproc3/perRoi-b_integ --ndev 4
```


