# coding: utf-8
from scipy.stats import linregress
from iotbx.reflection_file_reader import any_reflection_file
from pylab import *
import h5py
import glob
import sys
import os
from scipy.ndimage import center_of_mass, label
from joblib import Parallel, delayed


from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("dirnames", type=str, nargs="+", help="hopper_process output folders")
parser.add_argument("--labels", type=str, nargs="+", default=None,  help="plot legend labels")
parser.add_argument("--colors", type=str, nargs="+", default=None,  help="plot colors")
parser.add_argument("--j", help="number of args", default=8, type=int)
parser.add_argument("--nbins", help="number of resolution bins", type=int, default=15)
args = parser.parse_args()

if args.labels is not None:
    assert len(args.labels)==len(args.dirnames)
if args.colors is not None:
    assert len(args.colors)==len(args.dirnames)

NJ = args.j
NMAX = 9999
SCRIPT_DIR = os.path.dirname(__file__)

def main(jid, NJ, fnames):
    all_Hi = []
    all_sigZ = []
    for i_f, f in enumerate(fnames):
        if i_f % NJ != jid:
            continue
        if jid==0:
            print("Loading data from %d / %d" % (i_f+1, len(fnames)))
        D = np.load(f, allow_pickle=True)[()]
        params = D.params
        sigma_rdout = params.refiner.sigma_r /  params.refiner.adu_per_photon
        Hi = D.Hi
        all_Hi += list(Hi)
        roi_ids = set(D.roi_id)
        for i_roi in range(len(Hi)):
            sel = D.roi_id == i_roi
            mod = D.best_model[sel] + D.all_background[sel]
            dat = D.all_data[sel]
            Zscore = (mod-dat) / np.sqrt(mod+sigma_rdout**2)

            trus = D.all_trusted[sel]
            sigZ = np.std(Zscore[trus])
            all_sigZ.append(sigZ)
        if i_f > NMAX:
            break
    return all_Hi, all_sigZ


figure(1)
ax = gca()
gcf().set_size_inches((4,4))
figure(2)
gcf().set_size_inches((4,4))
ax2 = gca()

Z = []
RES = []
H = []
for i_dir, dirname in enumerate(args.dirnames):
    MTZ = os.path.join(SCRIPT_DIR, "../AD_SE_13_222/100shuff.mtz")
    F = any_reflection_file(MTZ).as_miller_arrays()[0]
    F = F.expand_to_p1()
    fnames = glob.glob("%s/modelers/*.npy" % dirname)
    print("Found %d files in %s" % (len(fnames), dirname))
    assert fnames, "Need some files to proceed"
   
    results = Parallel(n_jobs=NJ)(delayed(main)(j, NJ, fnames) for j in range(NJ))
    
    all_Hi, all_sigZ = [],[]
    for h,z in results:
        all_Hi += h
        all_sigZ += z 
    
    print("TOTAL SIGZs: %d" % len(all_sigZ))

    Dsp = F.d_spacings()
    resmap = {h: v for h, v in zip(Dsp.indices(), Dsp.data())}
    all_d = np.array([resmap[h] for h in all_Hi if h in resmap])
    all_z = np.array([all_sigZ[i] for i,h in enumerate(all_Hi) if h in resmap])
    Z.append(all_z)
    RES.append(all_d)
    H.append( all_Hi)
    res_bins = [d[0] for d in np.array_split(np.sort(all_d), args.nbins)] + [max(all_d)]

    bin_id = np.digitize(all_d, bins=res_bins)
    all_d_binned = []
    all_z_binned = []
    for i in range(1, args.nbins+1):
        sel = bin_id==i
        all_d_binned.append( mean(all_d[sel]))
        all_z_binned.append( mean(all_z[sel]))

    lab = dirname
    if args.labels is not None:
        lab = args.labels[i_dir]

    color = None
    if args.colors is not None:
        color=args.colors[i_dir]
    ax.plot( all_z_binned, marker='o', label=lab, color=color)
    bins = logspace( log10(min(all_z)), log10(max(all_z)), 50 )
    ax2.hist(all_z, bins=bins, label=lab, color=color) #, histtype='step')# alpha=0.777)
   
ax2.set_yscale("log")
ax2.set_xscale("log")
ax2.legend(prop={"size":9})
ax2.set_xlabel(r"$\sigma_Z$", fontsize=10)
ax2.set_ylabel("number of spots", fontsize=10)

ax.legend(prop={"size":9})
ax.set_xlabel(r"resolution ($\AA$)", fontsize=10)
ax.set_ylabel(r"$\langle\sigma_Z\rangle$", fontsize=10)

xlabs = ["%.1f-%.1f"% (x1,x2) for x1,x2 in zip(res_bins[:], res_bins[1:])]
xticks = list(range(len(xlabs)))
ax.set_xticks(xticks[::2])
ax.set_xticklabels(xlabs[::2], rotation=270)
for aa in (ax,ax2):
    aa.tick_params(labelsize=9)
    aa.grid(1, lw=0.5, ls='--', axis='both')
figure(1)
subplots_adjust(bottom=0.22, left=0.15,top=0.97 )
savefig("sigZ_lines.png", dpi=350)
figure(2)
subplots_adjust(bottom=0.15, left=0.15, right=.95, top=.95)
savefig("sigZ_hist.png", dpi=350)

show()
