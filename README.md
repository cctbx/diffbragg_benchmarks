<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>

# diffbragg_benchmarks

## hot links
* [NERSC stage1 example](#nersc_stage1)
* [STAGE2 benchmark](#stage2)


## Instructions for using diffBragg stage 1 scripts

### `simtbx.diffBragg.hopper`
**Use this script when you wish to refine models for pre-existing experiment lists and reflection tables**. One can use `*strong.refl`, `*indexed.refl` or `*integrate.refl` - it doesnt matter at this stage. Further, one does not need to index the reflections, one just needs the columns `xyzobs.px.value` and `panel` both present in the reflection table. 

### `diffBragg.hopper_process`
This is the diffBragg analog of the `dials.stills_process` command line utility. **Use this script when you have experiment image files that you need to process.** Importantly, it overrides the `refine()` method of `dials.stills_process` in order to launch a diffBragg refinement on the indexed reflections. 

<a name="nersc_stage1"></a>
#### NERSC example

After gaining access to the NERSC project m3562, one can access the relevant data. Prior to running this example, be sure to download all git lfs files, unpack the tarball in the `AD_SE_13_222` subfolder, and run the brief set of proprocessing commands discussed [below](#stage2), as well as running the following from the `cyto_stage1` folder:

```
# unpacks the image masks
cd cyto_stage1/
libtbx.python trusted_as_flex.py
```

We have a 10-run set of cytochrome diffraction collected on the Jungfrau 16M camera at the SwissFEL lightsource.

To process the raw images, one issues the command

```
export IMGFILE=/global/cfs/cdirs/m3562/der/master_files/run_000795.JF07T32V01_master.h5
export ODIR=test_run
DIFFBRAGG_USE_CUDA=1 srun -N1 --tasks-per-node=8 -c2  diffBragg.hopper_process proc.phil $IMGFILE num_devices=8 output.output_dir=$ODIR
```

where the parameter file `proc.phil` contains a bulk of settings:

<details>
  <summary>Example proc.phil</summary>

```
silence_dials_loggers=True
partial_correct=False
refspec=None
save_modelers=True
reidx_obs=True
input.reference_geometry=./AD_SE_13_222/data_222/Jungfrau_model.json

diffBragg {
  spectrum_from_imageset = True
  downsamp_spec {
    delta_en = 0.250000
  }
  ignore_existing = True
  betas {
    detz_shift = 1e-08
    ucell = 0.001 0.001
    RotXYZ = 1e-05
    Nabc = 50.000000 50.000000 50.000000
    G = 10.000000
  }
  centers {
    ucell = 78.61 265.12
    Nabc = 50.000000 50.000000 37.500000
    G = 10.000000
  }
  method = "L-BFGS-B"
  sigmas {
    RotXYZ = 0.001 0.001 0.001
  }
  init {
    Nabc = 50.000000 50.000000 37.500000
    G = 10.000000
  }
  mins {
    detz_shift = -1.5
    RotXYZ = -15 -15 -15
  }
  maxs {
    detz_shift = 1.5
    Nabc = 1600 1600 1600
    RotXYZ = 15 15 15
    G = 100000
  }
  fix {
    detz_shift = True
  }
  ucell_edge_perc = 15
  ucell_ang_abs = 1
  simulator {
    oversample = 4
    structure_factors {
      mtz_name = "./AD_SE_13_222/100shuff.mtz"
      mtz_column = "F(+),SIGF(+),F(-),SIGF(-)"
    }
    beam {
      size_mm = 0.001
    }
    detector {
      force_zero_thickness = True
    }
  }
  refiner {
    sigma_r = 10
    adu_per_photon = 9.481
  }
  roi {
    fit_tilt = True
    pad_shoebox_for_background_estimation = 10
    reject_edge_reflections = False
    hotpixel_mask = "./cyto_stage1/newmask_withbad.pkl"
    fit_tilt_using_weights = False
  }
}

spotfinder {
  threshold.dispersion.gain=9.5
  filter.min_spot_size=3
  filter.d_min=1.6
  lookup.mask=./cyto_stage1/trusted_Py3.pkl
}

indexing {
  refinement_protocol.d_min_start=2.3
  stills {
    refine_candidates_with_known_symmetry=True    
    candidate_outlier_rejection=False
    rmsd_min_px=7
  }
  known_symmetry{
    unit_cell=78.6763,78.6763,265.49,90,90,120
    space_group=P6522
  }
}

integration{
  debug.output=True
  lookup.mask = ./cyto_stage1/trusted_Py3.pkl
  summation.detector_gain=9.5
  background {
    algorithm = simple
    simple {
      outlier.algorithm = plane
      outlier.plane.n_sigma = 4.0
      model.algorithm = linear2d
    }
  }
}
dispatch.integrate=True
profile.gaussian_rs.centroid_definition=com
```
</details>

Once the refinement has proceeded for about an hour, pause it and analyze the results. There are the usual output files produced by `stills_process`, but there are additional files now in the `$ODIR/modelers` and `$ODIR/pandas` folders. See [TODO link] for a description of the various output files. 

Here we provide a few simple scripts for viewing the stage 1 results. The first looks at prediction offsets (distance from predicted spots to observed spots). Execute this command as follow

```
libtbx.python cyto_stage1/pred_offsets.py $ODIR
```

<p align="center">
<img src="https://gitlab.com/cctbx/diffbragg_benchmarks/uploads/2149d7e0f79457363eda9b980bd09181/overall.png" />
</p>

<p align="center">
<img src="https://gitlab.com/cctbx/diffbragg_benchmarks/uploads/0475ca5d64b48942690905c538c9354e/radial.png" />
</p>

<p align="center">
<img src="https://gitlab.com/cctbx/diffbragg_benchmarks/uploads/9ee7c6cac983aac504358b0895570dbd/tang.png" />
</p>

The above plots compare the spot predictions from DIALS and optimized diffBragg. 

Comparing prediction offsets, however, is a bit misleading. For example, in DIALS, if one turns on indexing refinement with `candidate_outlier_rejection=True`, then one will end up with fewer reflections that are all predicted fairly well (because DIALS will throw out the ones that are predicted poorly) . One will find only small improvements in prediction accuracy upon feeding these indexed reflections through diffbragg refinement. The real benefit of diffbragg is its ability to optimize the prediction of would-be outlier spots. Therefore, a better metric of comparison is say, let either program do its absolute best, then reindex all strong spot observations post-refinement, and count the total number of indexed spots, predicted within 1 pixel of the observations. (TODO)

#### pixel model accuracy
diffBragg is a pixel modeling program, and it's informative to visualize the agreement between modeled pixel intensity and measured pixel intensity. We define the quantity


$`
Z_i = (\text{data}_i - \text{model}_i) / \sigma_i
`$

for each pixel $`i`$, where $`\sigma_i`$ is the error estimate for the pixel intensity. For the pixels in each modeled shoebox, we compute the standard deviation of the Z-scores, $`\sigma_Z`$, and then we plot the average $`\sigma_Z`$ across all modeled shoeboxes, as a function of resolution:

```
# pass multiple ODIR args to compare refinements
libtbx.python cyto_stage1/sigz_vs_res_modelers.py $ODIR 
```

<p align="center">
<img src="https://gitlab.com/cctbx/diffbragg_benchmarks/uploads/64b4a4a14c7bc35af42ab695a43b71ed/sigz_dum.png" />
</p>

One can see that high resolution spots are modeled with slightly better Z-scores compared with lower resolution spots, something currently under investiation. It could be model inadequacy, or it could be something simple and tunable, say the sizes of the shoeboxes fed into diffBragg refinement. In any case, a $`\sigma_Z`$ of 1 is considered ideal.

<a name="stage2"></a>
## Instructions for running ADSE 222

First, install CCTBX project and activate your environment. 

```
source /path/to/cctbx/build/setpaths.sh
```

Then, download this repository, and download the big data using ```git-lfs```. Unpack the tarball

```
tar -xzvf data_222.tar.gz
```

Next, be sure to install the pandas Python module

```
libtbx.python -m pip install pandas
```

and then run the script

```
./complete_paths.py /path/to/data_222/
```

where the single argument is the unpacked ```data_222``` folder, and can be a relative path. This will produce the input file for running the stage 2 benchmark, which will be printed to stdout upon the scripts completion.

As a final setup step, one should convert the provided hkl file into an mtz format:

```
iotbx.reflection_file_converter  --unit_cell="77.856,77.856,263.615,90,90,120" --space_group=P6522 --mtz=100shuff.mtz --mtz_root_label=F 100shuff.hkl=amplitudes
```

The additional ```=amplitudes``` string attached to the input file name specifies that it contains structure factor amplitudes. This writes the mtz file 100shuff.mtz.

Next, one can run the benchmark. A shell script ```run.sh``` for running on Perlmutter is provided as a template. Optionally, install the line profiler using pip before running the benchmark

```
libtbx.python -m pip install line_profiler
```

Before running the mutli-node job, one can run a quick 2-shot test on the login node. One can trim the input pandas dataframe like so, using Python

```python 
import pandas
df = pandas.read_pickle("data_222/8_stage2_7_gathered_trimmed.pkl")
df2 = df.iloc[:2]
df2.to_pickle("2.pkl")
```


```
OMP_NUM_THREADS=16 simtbx.diffBragg.stage_two data_222.phil  io.output_dir=tests pandas_table=2.pkl num_devices=1 logfiles=True profile=True prep_time=1 logging.disable=False max_calls=[501] save_model_freq=250 refiner.load_data_from_refl=True refiner.reference_geom=data_222/Jungfrau_model.json structure_factors.mtz_name=100shuff.mtz structure_factors.mtz_column="F(+),SIGF(+),F(-),SIGF(-)" min_multiplicity=1
```

Note the file ```data_222.phil``` exists in this repo (```AD_SE_13_2222``` folder). Experiment with ```OMP_NUM_THREADS``` to see if OpenMP is installed correctly (```OMP_NUM_THREADS=1``` will take significantly longer per iteration). If the preceding command works, then the multi-node batch job should also work. To run the multi-node job, submit something akin to following SLURM script:

<details>
  <summary>example SLURM script</summary>

```bash
#!/bin/bash

#SBATCH -A m1759
#SBATCH -C gpu
#SBATCH -q regular
#SBATCH -t 90
#SBATCH -n 180
#SBATCH -c 2
#SBATCH --gpus-per-task 1

export PERL_NDEV=4  # number GPU per node
export odir=run_222_trial1  # can be whatever
export PANDA=data_222/8_stage2_7_gathered_trimmed.pkl
export GEOM=data_222/Jungfrau_model.json
DIFFBRAGG_USE_CUDA=1 srun -n180 -c2 simtbx.diffBragg.stage_two data_222.phil  io.output_dir=$odir pandas_table=$PANDA num_devices=$PERL_NDEV logfiles=True profile=True prep_time=90 logging.disable=False max_calls=[501] save_model_freq=250 refiner.load_data_from_refl=True refiner.reference_geom=$GEOM structure_factors.mtz_name=100shuff.mtz structure_factors.mtz_column="F(+),SIGF(+),F(-),SIGF(-)"
```
</details>

### Making 100shuff.hkl

We use a phenix installation for this. E.g., on Mac OS, install [phenix](https://phenix-online.org/download/), and then set the environment, e.g. ```source /Applications/phenix-1.19-4092/build/setpaths.sh```. Then, download the PDB model which the ADSE 222 data corresponds to

```
iotbx.fetch_pdb 5wp2
```

Next, use phenix to perturb the model 100 times with a large RMSD

```bash
mkdir pdb_shakes
for i in {0..99}
do 
  phenix.pdbtools sites.shake=7 5wp2.pdb  output.filename=pdb_shakes/$i.pdb
done
```

Then, generate the structure factors for each perturbed model

```bash
for i in {0..99}
do 
  libtbx.python  Fcalc_shakes.py  pdb_shakes/$i.pdb 
done
```

where ```Fcalc_shakes.py``` contains the following code:

<details>
  <summary>Fcalc_shakes.py</summary>

```python
#!/usr/bin/env libtbx.python

"""Fcalc_shakes.py"""

import sys
def get_complex_fcalc_from_pdb(pdb_file, high_res=2.1, unit_cell_length_tolerance=0.1,
                               fp_test=0, fdp_test=0, test_elem="Fe", wavelength=None):
  from iotbx import file_reader
  import mmtbx.command_line.fmodel
  import mmtbx.utils
  import math
  from cctbx.eltbx import henke

  pdb_in = file_reader.any_file(pdb_file, force_type="pdb")
  pdb_in.assert_file_type("pdb")
  xray_structure = pdb_in.file_object.xray_structure_simple()
  xray_structure.show_summary()
  for sc in xray_structure.scatterers():
    if wavelength is not None:
      expected_henke = henke.table(sc.element_symbol()).at_angstrom(wavelength)
      sc.fp = expected_henke.fp()
      sc.fdp = expected_henke.fdp()
    #if sc.element_symbol() == test_elem:
    #  sc.fp = fp_test
    #  sc.fdp = fdp_test
  phil2 = mmtbx.command_line.fmodel.fmodel_from_xray_structure_master_params
  params2 = phil2.extract()
  params2.high_resolution = high_res / math.pow(
    1 + unit_cell_length_tolerance, 1 / 3)
  params2.fmodel.k_sol = 0.435
  params2.fmodel.b_sol = 46.0
  params2.structure_factors_accuracy.algorithm = 'fft'
  f_model = mmtbx.utils.fmodel_from_xray_structure(
    xray_structure=xray_structure,
    f_obs=None,
    add_sigmas=False,
    params=params2).f_model
  if True:
    f_model = f_model.generate_bijvoet_mates()

  return f_model


if __name__=="__main__":
  
  F = get_complex_fcalc_from_pdb(sys.argv[1], high_res=1.5)
  F = F.as_amplitude_array()
  F.as_mtz_dataset(column_root_label="F")\
    .mtz_object().write(sys.argv[1]+".mtz")
```
</details>

Finally, combine the structure factors:

<details>
  <summary>Combining structure factors script </summary>

```python
import glob
from iotbx.reflection_file_reader import any_reflection_file
import numpy as np

fnames = glob.glob("pdb_shakes/*mtz")
datas = {}
indices = {}
for f in fnames:
    F = any_reflection_file(f).as_miller_arrays()[0]
    datas[f] = F.data()
    indices[f] = F.indices()

# sanity check all indices are the same
for j in range(1,100):
  print(j)
  for i,h in enumerate(indices[fnames[0]]):
    assert indices[fnames[j]][i]==h

D = np.array([datas[d].as_numpy_array() for d in datas])
ave_D = D.mean(0)
mset = F.miller_set(F.indices(), True)
from cctbx import miller
from dials.array_family import flex
ma = miller.array(mset, flex.double(ave_D)).set_observation_type_xray_amplitude()
ma.as_mtz_dataset(column_root_label="F").mtz_object().write("100shuff.mtz")
```
<\details>

We used ```iotbx.reflection_file_converter``` to convert to shelx format to obtain 100shuff.hkl.
